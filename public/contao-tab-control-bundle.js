let tabs = {

    tabItem: "",
    tabNav: "",
    tabNavItems: "",
    tabNavLinks: "",
    tabActive: "active-tab",
    tabNavActive: "active-tab-nav",
    activeTab: null,

    init: () => {

        tabs.wrapper = document.querySelectorAll('.ce_tabs');
        tabs.tabItem = document.querySelectorAll('.tab-pane');
        tabs.tabNav = document.querySelectorAll('.ce_tabs-nav');
        tabs.tabNavItems = document.querySelectorAll('.ce_tabs-nav li');
        tabs.tabNavLinks = document.querySelectorAll('.ce_tabs-nav a.nav-link');

        if (tabs.tabItem.length > 0) {
            tabs.setEvents();
            tabs.rememberLastTab();
        }

        tabs.wrapper.forEach(function (wrapper) {
            wrapper.classList.add('loaded');
        });

    },

    setEvents: () => {

        tabs.tabNavLinks.forEach(function (tab) {
            tab.addEventListener('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                var tabIndex = tab.getAttribute('aria-controls');
                tabs.changeTab(tabIndex);
                sessionStorage.setItem(tab.closest('.ce_tabs-nav').id, tabIndex);
            });
        });
    },

    changeTab: (tabIndex) => {
        tabs.tabItem.forEach(function (item) {
            item.classList.remove('active-tab');
        });
        tabs.tabNavLinks.forEach(function (navitem) {
            if(navitem.getAttribute('aria-controls') !== tabIndex) {
                navitem.classList.remove(tabs.tabNavActive);
            }else{
                navitem.classList.add(tabs.tabNavActive);
            }
        });
        tabs.setActiveTab(tabIndex);

        if (window.ppSlider && Array.isArray(window.ppSlider)) {
            window.ppSlider.forEach(function (slider) {
                slider.update();
            });
        }
    },

    setActiveTab: (tabIndex) => {
        var selectedTab = document.getElementById(tabIndex);
        selectedTab.classList.add('active-tab');
    },

    rememberLastTab: () => {
        tabs.tabNav.forEach(function (nav) {
            if ('true' === nav.dataset.rememberLastTab) {
                let savedTabPosition = sessionStorage.getItem(nav.id);
                if (null !== savedTabPosition) {
                    tabs.changeTab(savedTabPosition);
                }
            }
        });
    }
}

window.addEventListener('DOMContentLoaded', function () {
    tabs.init();
});
