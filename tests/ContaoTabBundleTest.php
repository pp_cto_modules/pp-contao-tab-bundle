<?php

/*
 * This file is part of Contao Simple SVG Icons Bundle.
 *
 * (c) slashworks
 *
 * @license LGPL-3.0-or-later
 */

namespace ppag\TabBundle\Tests;

use PHPUnit\Framework\TestCase;
use ppag\TabBundle\TabBundle;

class ContaoTabBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new TabBundle();
        $this->assertInstanceOf('ppag\TabBundle\ContaoTabBundleTest', $bundle);
    }
}
