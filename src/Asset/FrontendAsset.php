<?php

/*
 * Copyright (c) 2021 Heimrich & Hannot GmbH
 *
 * @license LGPL-3.0-or-later
 */

namespace ppag\TabBundle\Asset;

use Contao\System;

use HeimrichHannot\UtilsBundle\Util\Utils;

class FrontendAsset
{
    /**
     * @var ContainerUtil
     */
    private Utils $utils;
    /**
     * FrontendAsset constructor.
     */
    public function __construct(Utils $utils)
    {
        $this->utils = $utils;
    }

    public function addFrontendAsset()
    {

        //olny backend
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();
        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request)) {
            $GLOBALS['TL_CSS']['ppag-tab-bundle_backend'] = 'bundles/tab/contao-tab-control-bundle-backend.css';
        } else {
            $GLOBALS['TL_JAVASCRIPT']['ppag-tab-bundle'] = 'bundles/tab/contao-tab-control-bundle.js';
        }
    }
}
