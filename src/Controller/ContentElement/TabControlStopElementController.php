<?php

/*
 * Copyright (c) 2021 Heimrich & Hannot GmbH
 *
 * @license LGPL-3.0-or-later
 */

namespace ppag\TabBundle\Controller\ContentElement;

use Contao\BackendTemplate;
use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use HeimrichHannot\UtilsBundle\Util\Utils;
use ppag\TabBundle\Asset\FrontendAsset;
use ppag\TabBundle\Helper\StructureTabHelper;
use HeimrichHannot\UtilsBundle\Util\Container\ContainerUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @ContentElement(TabControlStopElementController::TYPE,category="tabs",template="ce_tabcontrol_stop")
 */
class TabControlStopElementController extends AbstractContentElementController
{
    const TYPE = 'tabcontrolStop';

    /**
     * @var ContainerUtil
     */
    protected $containerUtil;

    /**
     * @var StructureTabHelper
     */
    protected $structureTabHelper;

    /**
     * @var FrontendAsset
     */
    protected $frontendAsset;

    public function __construct(Utils $containerUtil, StructureTabHelper $structureTabHelper, FrontendAsset $frontendAsset)
    {
        $this->containerUtil = $containerUtil;
        $this->structureTabHelper = $structureTabHelper;
        $this->frontendAsset = $frontendAsset;
    }

    protected function getResponse(Template $template, ContentModel $element, Request $request): Response
    {
        if ($this->containerUtil->container()->isBackend()) {
            $template = new BackendTemplate('be_tabs_control');
        }

        $tabs = $this->structureTabHelper->getTabDataForContentElement($element->id, $element->pid, $element->ptable);

        $template->id = $element->id;
        $template->tabs = $tabs;
        $template->isStopElement = true;

        return $template->getResponse();
    }
}
