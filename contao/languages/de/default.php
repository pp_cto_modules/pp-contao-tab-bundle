<?php

$GLOBALS['TL_LANG']['CTE']['tabs'] = 'Tabs';

$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlStartElementController::TYPE]     = ['Tabs Start', ''];
$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlSeparatorElementController::TYPE] = ['Tabs Trenner', ''];
$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlStopElementController::TYPE]      = ['Tabs Endelement', ''];
