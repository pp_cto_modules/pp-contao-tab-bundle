<?php

$GLOBALS['TL_LANG']['CTE']['tabs'] = 'Tabs';

$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlStartElementController::TYPE]     = ['Tabs start', ''];
$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlSeparatorElementController::TYPE] = ['Tabs separator', ''];
$GLOBALS['TL_LANG']['CTE'][\ppag\TabBundle\Controller\ContentElement\TabControlStopElementController::TYPE]      = ['Tabs end element', ''];
