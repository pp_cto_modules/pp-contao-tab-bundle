<?php

use ppag\TabBundle\Controller\ContentElement\TabControlStartElementController;
use ppag\TabBundle\Controller\ContentElement\TabControlSeparatorElementController;
use ppag\TabBundle\Controller\ContentElement\TabControlStopElementController;
/**
 * Content elements
 */
$GLOBALS['TL_WRAPPERS']['start'][]     = TabControlStartElementController::TYPE;
$GLOBALS['TL_WRAPPERS']['separator'][] = TabControlSeparatorElementController::TYPE;
$GLOBALS['TL_WRAPPERS']['stop'][]      = TabControlStopElementController::TYPE;
